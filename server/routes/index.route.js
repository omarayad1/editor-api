import express from 'express';
import fileRoutes from './file.route';

const router = express.Router(); // eslint-disable-line new-cap

router.use('/file', fileRoutes);

export default router;
