import express from 'express';
import fileCtrl from '../controllers/file.controller';

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .get(fileCtrl.getFiles)

router.route('/:file')
  .put(fileCtrl.editFile);
  
export default router;
