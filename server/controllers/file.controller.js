import fs from 'fs';
import async from 'async';

function getFiles(req, res, next) {
  const data = {};
  fs.readdir('./files', (err, files) => {
    if (err) return next(err);
    async.eachSeries(files, (file, c) => {
      fs.readFile(`./files/${file}`, 'utf-8', (err, content) => {
        if (err) return next(err);
        data[file] = content;
        c();
      })
    }, () => res.json(data));
  })
}

function editFile(req, res, next) {
  const { file } = req.params;
  const { data } = req.body;
  fs.writeFile(`./files/${file}`, data, err => {
    if (err) return next(err);
    return res.json({[file]: data});
  })
}

export default { getFiles, editFile }
